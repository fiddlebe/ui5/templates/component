/* eslint-env es6 */
module.exports = function(grunt) {
	"use strict";

	// Log time how long tasks take
	grunt.initConfig({
	});


	//JSDoco parsing
	grunt.registerTask('jsdoc', 'parsing all jsdoco into one big json file', function(){
		grunt.log.writeln('Generating JSDoc file');
		var done = this.async();

		var jsdocx = require('jsdoc-x');

		jsdocx.parse({
			files:[
				'./webapp/**/*.js',
				"!webapp/test/**",
				"!webapp/localService/**"
			],
			excludePattern:'(test|localService)',
			recurse:true,
			private:true,
			output:'./webapp/documentation.json',
			encoding:'utf-8'
		}, function (err, docs) {
			if (err) {
				grunt.log.writeln('JSDoc file generation failed with error:');
				grunt.log.writeln(err);
				done(false);
			} else {
				grunt.log.writeln('JSDoc file generated');
				done(true);
			}			
		});
	});
	// Build task
	grunt.registerTask("jsdoc", ["jsdoc"	]);
};
