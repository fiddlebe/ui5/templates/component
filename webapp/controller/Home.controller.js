sap.ui.define(
	[
		"templatepath/controller/BaseController"
	],
	function(BaseController,) {
		"use strict";

		/**
		 * @name	ui5expresstemplate.controller.Home
		 * @alias 	ui5expresstemplate.controller.Home
		 * @constructor
		 * @public
		 * @extends sap.ui.core.mvc.Controller
		 * @class
		 * The HomeController is used as controller for the Home view.<br/>
		 **/
		const HomeController = BaseController.extend(
			"ui5expresstemplate.controller.Home",
			/** @lends ui5expresstemplate.controller.Home.prototype */
			{
				constructor: function() {}
			}
		);

		/**
		 * @memberof ui5expresstemplate.controller.Home
		 * @method onInit
		 * @public
		 * @instance
		 */
		HomeController.prototype.onInit = function() {
		    BaseController.prototype.onInit.apply(this,arguments);
		};

		return HomeController;
	}
);
