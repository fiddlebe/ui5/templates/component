sap.ui.define(
	[
		"sap/ui/model/json/JSONModel", 
		"sap/ui/Device"
	], 
	function(JSONModel,Device) {
		"use strict";

		const models = {};

		models.createDeviceModel= function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		};
		
		return models;
	}
);
