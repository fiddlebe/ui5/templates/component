sap.ui.define(
	[], 
	function() {
    	"use strict";
    
    	/**
    	 * @name 	ui5expresstemplate.util.Formatter
    	 * @alias 	ui5expresstemplate.util.Formatter
    	 * @constructor
    	 * @public
    	 * @class
    	 * this is the formatter for the ui5expresstemplate app. it contains the different formatting routines that are used in the views.<br/>
    	 * Add your methods onto the Formatter constant.
    	 */
    	const Formatter = {};
    
    	return Formatter;
    }
);
